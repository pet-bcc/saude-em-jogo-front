import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {IndexComponent} from './index/index.component';
import {GameMainPageComponent} from './game/game-main-page/game-main-page.component';
import {ContratarMedicosComponent} from './game/medico/contratar-medicos/contratar-medicos.component';
import {ComprarModulosComponent} from './game/modulo/comprar-modulos/comprar-modulos.component';
import {MeusMedicosComponent} from './game/medico/meus-medicos/meus-medicos.component';
import {MeusModulosComponent} from './game/modulo/meus-modulos/meus-modulos.component';
import {FazerEmprestimoComponent} from './game/emprestimo/fazer-emprestimo/fazer-emprestimo.component';
import {MeusEmprestimosComponent} from './game/emprestimo/meus-emprestimos/meus-emprestimos.component';
import {EstatisticaComponent} from './game/estatistica/estatistica.component';
import {GameGuard} from './game/game.guard';

// @ts-ignore
const routes: Routes = [
  {path: '', component: IndexComponent},
  {
    path: 'game',
    component: GameMainPageComponent,
    children: [
      {path: '', component: EstatisticaComponent},

      {path: 'contratar-medicos', component: ContratarMedicosComponent, canActivate: [GameGuard]},
      {path: 'comprar-modulos', component: ComprarModulosComponent, canActivate: [GameGuard]},
      {path: 'fazer-emprestimo', component: FazerEmprestimoComponent, canActivate: [GameGuard]},

      {path: 'meus-medicos', component: MeusMedicosComponent, canActivate: [GameGuard]},
      {path: 'meus-modulos', component: MeusModulosComponent, canActivate: [GameGuard]},
      {path: 'meus-emprestimos', component: MeusEmprestimosComponent, canActivate: [GameGuard]},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
