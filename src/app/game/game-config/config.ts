export interface Config {
  id: number;
  nome: string;
  porcentagem_reembolso_modulo: number;
  emprestimos_possiveis: string[];
  tempo_minimo_demitir_medico: number;
  tempo_rodada: number;
  tempo_maximo_para_venda_modulo: number;
  caixa_inicial: number;
  juros_do_emprestimo: number;
  mes_pagar_emprestimo: number;
}
