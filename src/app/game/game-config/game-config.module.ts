import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GameConfigService} from './game-config.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    GameConfigService
  ]
})
export class GameConfigModule {
}
