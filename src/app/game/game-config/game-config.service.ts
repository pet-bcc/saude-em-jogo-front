import { Injectable } from '@angular/core';
import {ApiService} from '../../shared/services/api.service';
import {Observable} from 'rxjs';
import {Config} from './config';

@Injectable({
  providedIn: 'root'
})
export class GameConfigService {

  constructor(private _api: ApiService) { }

  getOficialConfig(): Observable<Config> {
    return this._api.get('get_config/1') as Observable<Config>;
  }

  getConfig(config: number): Observable<Config> {
    return this._api.get('get_config/' + config) as Observable<Config>;
  }

  getAllConfig(): Observable<{ nome: string, id: number }[]> {
    return this._api.get('all_configs') as Observable<{ nome: string, id: number }[]>;
  }
}
