import {AfterViewInit, Component, EventEmitter, OnInit, ViewChild} from '@angular/core';
import {GameService} from '../game.service';
import {CountdownComponent} from 'ngx-countdown';

@Component({
  selector: 'sj-game-main-page',
  templateUrl: './game-main-page.component.html',
  styleUrls: ['./game-main-page.component.scss']
})
export class GameMainPageComponent implements OnInit, AfterViewInit {
  jogo_pronto = false;
  private last_month = false;
  constructor(private game: GameService) { }
  @ViewChild(CountdownComponent) counter: CountdownComponent;

  get tempo_rodada() {
    return this.game.configuration.tempo_rodada * 60;
  }

  get month() {
    return this.game.current_round;
  }

  get finished() {
    return this.game.finished;
  }

  ngOnInit() {
    this.game.startGame(
      _ => this.jogo_pronto = _
    );
  }

  onFinish() {
    this.game.nextMonth();
  }

  onEvent(event) {
    if ( event.action === 'finished' && !this.last_month ) {
      this.counter.restart();
      if ( this.month === 8 ) {
        this.last_month = true;
      }
    }
  }

  ngAfterViewInit(): void {
  }
}
