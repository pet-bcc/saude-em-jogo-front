import {Injectable} from '@angular/core';
import {ApiService} from '../../shared/services/api.service';
import {Observable} from 'rxjs';
import {Medico} from './medico';

@Injectable({
  providedIn: 'root'
})
export class MedicoService {

  constructor(private _api: ApiService) {
  }

  getAll(): Observable<{ medicos: Medico[] }> {
    return this._api.get('all_medicos') as Observable<{ medicos: Medico[] }>;
  }
}
