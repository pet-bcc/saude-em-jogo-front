export interface Medico {
  id: number;
  rodada_aquisicao?: number;
  nome: string;
  expertise: string;
  salario: number;
  pontualidade: string;
  atendimento: string;
}
