import { Component, OnInit } from '@angular/core';
import {GameService} from '../../game.service';

@Component({
  selector: 'sj-contratar-medicos',
  templateUrl: './contratar-medicos.component.html',
  styleUrls: ['./contratar-medicos.component.scss']
})
export class ContratarMedicosComponent implements OnInit {

  constructor(private game: GameService) { }

  get medicos() {
    return this.game.medicos_available;
  }

  ngOnInit() {
  }

}
