import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContratarMedicosComponent } from './contratar-medicos.component';

describe('ContratarMedicosComponent', () => {
  let component: ContratarMedicosComponent;
  let fixture: ComponentFixture<ContratarMedicosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContratarMedicosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContratarMedicosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
