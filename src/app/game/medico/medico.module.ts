import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MedicoService} from './medico.service';
import { MedicoCardComponent } from './medico-card/medico-card.component';
import {AppMaterialModule} from '../../app-material.module';
import {SharedModule} from '../../shared/shared.module';
import { MedicoAllComponent } from './medico-all/medico-all.component';
import { ContratarMedicosComponent } from './contratar-medicos/contratar-medicos.component';
import { MeusMedicosComponent } from './meus-medicos/meus-medicos.component';

@NgModule({
  declarations: [
    MedicoCardComponent,
    MedicoAllComponent,
    ContratarMedicosComponent,
    MeusMedicosComponent
  ],
  imports: [
    CommonModule,
    AppMaterialModule,
    SharedModule
  ],
  exports: [
    MedicoAllComponent
  ],
  providers: [
    MedicoService
  ]
})
export class MedicoModule {
}
