import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeusMedicosComponent } from './meus-medicos.component';

describe('MeusMedicosComponent', () => {
  let component: MeusMedicosComponent;
  let fixture: ComponentFixture<MeusMedicosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeusMedicosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeusMedicosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
