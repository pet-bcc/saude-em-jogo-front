import { Component, OnInit } from '@angular/core';

import {GameService} from '../../game.service';

@Component({
  selector: 'sj-meus-medicos',
  templateUrl: './meus-medicos.component.html',
  styleUrls: ['./meus-medicos.component.scss']
})
export class MeusMedicosComponent implements OnInit {

  constructor(private game: GameService) { }

  get medicos() {
    return this.game.medicos_contracted;
  }

  ngOnInit() {
  }

}
