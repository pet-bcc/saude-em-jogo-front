import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicoAllComponent } from './medico-all.component';

describe('MedicoAllComponent', () => {
  let component: MedicoAllComponent;
  let fixture: ComponentFixture<MedicoAllComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicoAllComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicoAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
