import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {animate, style, transition, trigger} from '@angular/animations';

import {Medico} from '../medico';

@Component({
  selector: 'sj-medico-all',
  templateUrl: './medico-all.component.html',
  styleUrls: ['./medico-all.component.scss'],
  animations: [
    trigger('items', [
      // cubic-bezier for a tiny bouncing feel
      transition(':enter', [
        style({ transform: 'scale(0.5)', opacity: 0 }),
        animate('1s cubic-bezier(.8,-0.6,0.2,1.5)',
          style({ transform: 'scale(1)', opacity: 1 }))
      ]),
      transition(':leave', [
        style({ transform: 'scale(1)', opacity: 1, height: '*' }),
        animate('1s cubic-bezier(.8,-0.6,0.2,1.5)',
          style({ transform: 'scale(0.5)', opacity: 0, height: '0px', margin: '0px' }))
      ]),
    ])
  ]
})
export class MedicoAllComponent implements OnInit, OnChanges {
  @Input() medicos: Medico[];
  @Input() isMine = false;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );
  constructor(private breakpointObserver: BreakpointObserver) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.medicos && !changes.medicos.isFirstChange()) {
      this.medicos = changes.medicos.currentValue;
    }
    if (changes.isMine && !changes.isMine.isFirstChange()) {
      this.isMine = changes.isMine.currentValue;
    }
  }

}
