import {Component, Input, OnInit} from '@angular/core';

import {Medico} from '../medico';
import {GameService} from '../../game.service';

@Component({
  selector: 'sj-medico-card',
  templateUrl: './medico-card.component.html',
  styleUrls: ['./medico-card.component.scss']
})
export class MedicoCardComponent implements OnInit {
  @Input() medico: Medico;
  @Input() isMine: boolean;

  constructor(private game: GameService) { }

  toContractMedico(id: number) {
    this.game.toContractMedico(id);
  }

  toDismissMedico(id: number) {
    this.game.toDismissMedico(id);
  }

  canDismiss() {
    return (this.medico.rodada_aquisicao + this.game.configuration.tempo_minimo_demitir_medico) >= this.game.current_round;
  }

  ngOnInit() {
  }

  getBarValue(char: string) {
    switch (char) {
      case 'A' || 'a':
        return 100;
      case 'B' || 'b':
        return (100 / 3 * 2);
      case 'C' || 'c':
        return (100 / 3);
      default:
        return 0;
    }
  }
}
