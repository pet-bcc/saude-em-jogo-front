import {Component, OnDestroy, OnInit} from '@angular/core';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import {Label} from 'ng2-charts';
import {LogService} from '../../log/log.service';
import {GameService} from '../../game.service';
import {LogMes} from '../../log/models/log-mes';
import {Subscription} from 'rxjs';

@Component({
  selector: 'sj-preco-medio-tratamento-chart',
  templateUrl: './preco-medio-tratamento-chart.component.html',
  styleUrls: ['./preco-medio-tratamento-chart.component.scss']
})
export class PrecoMedioTratamentoChartComponent implements OnInit, OnDestroy {
  barChartOptions: ChartOptions = {
    responsive: true,
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  barChartLabels: Label[] = [];
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartData: ChartDataSets[] = [];

  private onNextMonth: Subscription;

  constructor(private log: LogService,
              private game: GameService) { }

  update() {
    let log: LogMes;
    if ( this.game.finished ) {
      log = this.log.getLog(this.game.current_round);
    } else {
      log = this.log.getLog(this.game.current_round - 1);
    }
    const valores: number[] = [];
    valores.push(log.valor_medio_atendimento.card);
    valores.push(log.valor_medio_atendimento.emerg);
    valores.push(log.valor_medio_atendimento.neuro);
    valores.push(log.valor_medio_atendimento.psico);
    valores.push(log.valor_medio_atendimento.pedi);
    this.barChartData = [];
    this.barChartData.push({
      data: valores, label: 'Preço Médio do Atendimento'
    });
    this.barChartLabels = [];
    this.barChartLabels.push('Cardiologia');
    this.barChartLabels.push('Emergência');
    this.barChartLabels.push('Neurologia');
    this.barChartLabels.push('Psicologia');
    this.barChartLabels.push('Pediatria');
  }

  ngOnInit() {
    this.update();
    this.onNextMonth = this.game.onNextMonth.subscribe( _ => this.update() );
  }

  ngOnDestroy(): void {
    this.onNextMonth.unsubscribe();
  }

}
