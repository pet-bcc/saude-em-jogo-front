import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrecoMedioTratamentoChartComponent } from './preco-medio-tratamento-chart.component';

describe('PrecoMedioTratamentoChartComponent', () => {
  let component: PrecoMedioTratamentoChartComponent;
  let fixture: ComponentFixture<PrecoMedioTratamentoChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrecoMedioTratamentoChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrecoMedioTratamentoChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
