import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoneyPerMonthChartComponent } from './money-per-month-chart.component';

describe('MoneyPerMonthChartComponent', () => {
  let component: MoneyPerMonthChartComponent;
  let fixture: ComponentFixture<MoneyPerMonthChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoneyPerMonthChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoneyPerMonthChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
