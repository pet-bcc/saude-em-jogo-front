import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import {Label} from 'ng2-charts';
import {LogService} from '../../log/log.service';
import {GameService} from '../../game.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'sj-money-per-month-chart',
  templateUrl: './money-per-month-chart.component.html',
  styleUrls: ['./money-per-month-chart.component.scss']
})
export class MoneyPerMonthChartComponent implements OnInit, OnDestroy {
  barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  barChartLabels: Label[] = [];
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartData: ChartDataSets[] = [];

  private onNextMonth: Subscription;

  constructor(private log: LogService, private game: GameService) {
  }

  update() {
    this.barChartLabels = [];
    const dinheiro: number[] = [];
    this.log.getAll().forEach(log => {
      dinheiro.push(log.dinheiro_acumulado);
      this.barChartLabels.push('Mês ' + log.referente_mes);
    });
    this.barChartData = [];
    this.barChartData.push({ data: dinheiro, label: 'Dinheiro no Mês' });
  }

  ngOnInit() {
    this.update();
    this.onNextMonth = this.game.onNextMonth.subscribe(_ => this.update() );
  }

  ngOnDestroy(): void {
    this.onNextMonth.unsubscribe();
  }

}
