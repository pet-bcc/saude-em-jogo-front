import {Component, OnDestroy, OnInit} from '@angular/core';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import {Label} from 'ng2-charts';
import {Subscription} from 'rxjs';
import {LogService} from '../../log/log.service';
import {GameService} from '../../game.service';
import {LogMes} from '../../log/models/log-mes';

@Component({
  selector: 'sj-lucro-por-modulo-chart',
  templateUrl: './lucro-por-modulo-chart.component.html',
  styleUrls: ['./lucro-por-modulo-chart.component.scss']
})
export class LucroPorModuloChartComponent implements OnInit, OnDestroy {
  barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  barChartLabels: Label[] = [];
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartData: ChartDataSets[] = [];

  private onNextMonth: Subscription;

  constructor(private log: LogService, private game: GameService) {
  }

  update() {
    this.barChartLabels = [];
    this.barChartLabels.push('Cardiologia');
    this.barChartLabels.push('Emergência');
    this.barChartLabels.push('Neurologia');
    this.barChartLabels.push('Psicologia');
    this.barChartLabels.push('Pediatria');
    const lucro: number[] = [];
    let log: LogMes;
    if ( this.game.finished ) {
      log = this.log.getLog(this.game.current_round);
    } else {
      log = this.log.getLog(this.game.current_round - 1);
    }
    lucro.push(log.ganhos.atendimento.card - log.gastos.custo_mensal.card);
    lucro.push(log.ganhos.atendimento.emerg - log.gastos.custo_mensal.emerg);
    lucro.push(log.ganhos.atendimento.neuro - log.gastos.custo_mensal.neuro);
    lucro.push(log.ganhos.atendimento.psico - log.gastos.custo_mensal.psico);
    lucro.push(log.ganhos.atendimento.pedi - log.gastos.custo_mensal.pedi);
    this.barChartData = [];
    this.barChartData.push({ data: lucro, label: 'Lucro no Mês' });
  }

  ngOnInit() {
    this.update();
    this.onNextMonth = this.game.onNextMonth.subscribe(_ => this.update() );
  }

  ngOnDestroy(): void {
    this.onNextMonth.unsubscribe();
  }

}
