import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LucroPorModuloChartComponent } from './lucro-por-modulo-chart.component';

describe('LucroPorModuloChartComponent', () => {
  let component: LucroPorModuloChartComponent;
  let fixture: ComponentFixture<LucroPorModuloChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LucroPorModuloChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LucroPorModuloChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
