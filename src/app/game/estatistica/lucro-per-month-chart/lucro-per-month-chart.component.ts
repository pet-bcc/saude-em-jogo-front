import {Component, OnDestroy, OnInit} from '@angular/core';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import {Label} from 'ng2-charts';
import {Subscription} from 'rxjs';
import {LogService} from '../../log/log.service';
import {GameService} from '../../game.service';

@Component({
  selector: 'sj-lucro-per-month-chart',
  templateUrl: './lucro-per-month-chart.component.html',
  styleUrls: ['./lucro-per-month-chart.component.scss']
})
export class LucroPerMonthChartComponent implements OnInit, OnDestroy {
  barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  barChartLabels: Label[] = [];
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartData: ChartDataSets[] = [];

  private onNextMonth: Subscription;

  constructor(private log: LogService, private game: GameService) {
  }

  update() {
    this.barChartLabels = [];
    const lucro: number[] = [];
    this.log.getAll().forEach(log => {
      lucro.push(log.lucro);
      this.barChartLabels.push('Mês ' + log.referente_mes);
    });
    this.barChartData = [];
    this.barChartData.push({ data: lucro, label: 'Lucro no Mês' });
  }

  ngOnInit() {
    this.update();
    this.onNextMonth = this.game.onNextMonth.subscribe(_ => this.update() );
  }

  ngOnDestroy(): void {
    this.onNextMonth.unsubscribe();
  }

}
