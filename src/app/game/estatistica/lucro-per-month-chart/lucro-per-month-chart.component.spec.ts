import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LucroPerMonthChartComponent } from './lucro-per-month-chart.component';

describe('LucroPerMonthChartComponent', () => {
  let component: LucroPerMonthChartComponent;
  let fixture: ComponentFixture<LucroPerMonthChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LucroPerMonthChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LucroPerMonthChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
