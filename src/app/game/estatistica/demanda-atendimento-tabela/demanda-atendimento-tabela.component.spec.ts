import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemandaAtendimentoTabelaComponent } from './demanda-atendimento-tabela.component';

describe('DemandaAtendimentoTabelaComponent', () => {
  let component: DemandaAtendimentoTabelaComponent;
  let fixture: ComponentFixture<DemandaAtendimentoTabelaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemandaAtendimentoTabelaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemandaAtendimentoTabelaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
