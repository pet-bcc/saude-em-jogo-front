import {Component, OnDestroy, OnInit} from '@angular/core';
import {LogService} from '../../log/log.service';
import {GameService} from '../../game.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'sj-demanda-atendimento-tabela',
  templateUrl: './demanda-atendimento-tabela.component.html',
  styleUrls: ['./demanda-atendimento-tabela.component.scss']
})
export class DemandaAtendimentoTabelaComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = ['classe_social', 'atendidos', 'demanda', 'porcentagem'];
  dataSources: { card: any[], emerg: any[], neuro: any[], psico: any[], pedi: any[] };
  curSource = 'card';

  private onNextMonth: Subscription;

  constructor(private log: LogService,
              private game: GameService) { }

  getSourceDataByModulo(modulo: string, atendidos: any) {
    const source: any[] = [];
    const classes = ['A', 'B', 'C', 'D', 'E'];
    classes.forEach(
      classe => {
        source.push({
          classe_social: classe,
          atendidos: atendidos[modulo][classe],
          demanda: atendidos.demanda[modulo][classe],
          porcentagem: (atendidos[modulo][classe] / atendidos.demanda[modulo][classe] * 100).toFixed(2)
        });
      }
    );
    return source;
  }

  updateDataSource() {
    let lastLog;
    if ( this.game.finished ) {
      lastLog = this.log.getLog(this.game.current_round).atendidos;
    } else {
      lastLog = this.log.getLog(this.game.current_round - 1).atendidos;
    }
    this.dataSources = {
      card: this.getSourceDataByModulo('card', lastLog),
      emerg: this.getSourceDataByModulo('emerg', lastLog),
      neuro: this.getSourceDataByModulo('neuro', lastLog),
      psico: this.getSourceDataByModulo('psico', lastLog),
      pedi: this.getSourceDataByModulo('pedi', lastLog)
    };
  }

  ngOnInit() {
    this.updateDataSource();
    this.onNextMonth = this.game.onNextMonth.subscribe( _ => this.updateDataSource() );
  }

  ngOnDestroy(): void {
    this.onNextMonth.unsubscribe();
  }

}
