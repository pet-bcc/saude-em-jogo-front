import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'sj-all-charts',
  templateUrl: './all-charts.component.html',
  styleUrls: ['./all-charts.component.scss']
})
export class AllChartsComponent implements OnInit {
  @Input() header: string;
  constructor() { }

  ngOnInit() {
  }

}
