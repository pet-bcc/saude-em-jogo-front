import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AllChartsComponent} from './all-charts/all-charts.component';
import {AppMaterialModule} from '../../app-material.module';
import {EstatisticaComponent} from './estatistica.component';
import {ChartsModule} from 'ng2-charts';
import {MoneyPerMonthChartComponent} from './money-per-month-chart/money-per-month-chart.component';
import {PrecoMedioTratamentoChartComponent} from './preco-medio-tratamento-chart/preco-medio-tratamento-chart.component';
import { DemandaAtendimentoTabelaComponent } from './demanda-atendimento-tabela/demanda-atendimento-tabela.component';
import { LucroPerMonthChartComponent } from './lucro-per-month-chart/lucro-per-month-chart.component';
import { LucroPorModuloChartComponent } from './lucro-por-modulo-chart/lucro-por-modulo-chart.component';

@NgModule({
  declarations: [
    AllChartsComponent,
    EstatisticaComponent,
    MoneyPerMonthChartComponent,
    PrecoMedioTratamentoChartComponent,
    DemandaAtendimentoTabelaComponent,
    LucroPerMonthChartComponent,
    LucroPorModuloChartComponent,
  ],
  imports: [
    CommonModule,
    AppMaterialModule,
    ChartsModule
  ],
  exports: []
})
export class EstatisticaModule {
}
