import { Component, OnInit } from '@angular/core';
import {ThemeService} from 'ng2-charts';
import * as config_theme from './chart_theme';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {GameService} from '../game.service';

@Component({
  selector: 'sj-estatistica',
  templateUrl: './estatistica.component.html',
  styleUrls: ['./estatistica.component.scss']
})
export class EstatisticaComponent implements OnInit {
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );
  constructor(private themeService: ThemeService,
              private game: GameService,
              private breakpointObserver: BreakpointObserver) { }

  ngOnInit() {
    this.themeService.setColorschemesOptions(config_theme.overrides);
  }

  isPrimeiroMes() {
    return this.game.current_round === 1;
  }

}
