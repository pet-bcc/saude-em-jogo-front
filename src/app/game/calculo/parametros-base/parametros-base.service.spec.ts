import { TestBed } from '@angular/core/testing';

import { ParametrosBaseService } from './parametros-base.service';

describe('ParametrosBaseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ParametrosBaseService = TestBed.get(ParametrosBaseService);
    expect(service).toBeTruthy();
  });
});
