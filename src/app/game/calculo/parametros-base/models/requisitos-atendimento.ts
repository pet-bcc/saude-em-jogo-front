export interface RequisitosAtendimento {
  maxPreco: number;
  minTecnologia: number;
  minConforto: number;
  minEspecialidade: number;
  minVelocidade: number;
}
