import { Injectable } from '@angular/core';
import {RequisitosAtendimento} from './models/requisitos-atendimento';

@Injectable({
  providedIn: 'root'
})
export class ParametrosBaseService {
  private _A: RequisitosAtendimento;
  private _B: RequisitosAtendimento;
  private _C: RequisitosAtendimento;
  private _D: RequisitosAtendimento;
  private _E: RequisitosAtendimento;

  constructor() { }
  // TODO get from database
  updateRequisitosAtendimento() {
    this._A = {
      maxPreco: 100,
      minTecnologia: 2.8,
      minConforto: 2.8,
      minEspecialidade: 2.7,
      minVelocidade: 2
    };
    this._B = {
      maxPreco: 75,
      minTecnologia: 2.3,
      minConforto: 2,
      minEspecialidade: 2,
      minVelocidade: 2
    };
    this._C = {
      maxPreco: 50,
      minTecnologia: 2,
      minConforto: 1,
      minEspecialidade: 2,
      minVelocidade: 2
    };
    this._D = {
      maxPreco: 25,
      minTecnologia: 2,
      minConforto: 1,
      minEspecialidade: 1,
      minVelocidade: 1
    };
    this._E = {
      maxPreco: 10,
      minTecnologia: 1,
      minConforto: 1,
      minEspecialidade: 1,
      minVelocidade: 1
    };
  }

  get requisitosAtendimento() {
    return {
      A: Object.assign({}, this._A),
      B: Object.assign({}, this._B),
      C: Object.assign({}, this._C),
      D: Object.assign({}, this._D),
      E: Object.assign({}, this._E)
    };
  }
}
