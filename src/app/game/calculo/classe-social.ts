export class ClasseSocial {
  card: number;
  emerg: number;
  psico: number;
  neuro: number;
  pedi: number;

  constructor(info: {
    card: { media: number, desvio: number },
    emerg: { media: number, desvio: number },
    psico: { media: number, desvio: number },
    neuro: { media: number, desvio: number },
    pedi: { media: number, desvio: number }
  }) {
    this.card = (Math.floor(Math.random() * 100) % (info.card.desvio * 2)) + (info.card.media - info.card.desvio);
    this.emerg = (Math.floor(Math.random() * 100) % (info.emerg.desvio * 2)) + (info.emerg.media - info.emerg.desvio);
    this.psico = (Math.floor(Math.random() * 100) % (info.psico.desvio * 2)) + (info.psico.media - info.psico.desvio);
    this.neuro = (Math.floor(Math.random() * 100) % (info.neuro.desvio * 2)) + (info.neuro.media - info.neuro.desvio);
    this.pedi = (Math.floor(Math.random() * 100) % (info.pedi.desvio * 2)) + (info.pedi.media - info.pedi.desvio);
  }
}
