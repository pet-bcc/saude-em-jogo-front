import {Injectable} from '@angular/core';
import {Modulo} from '../modulo/modulo';
import {Medico} from '../medico/medico';
import {ClasseSocial} from './classe-social';
import {RequisitosAtendimento} from './parametros-base/models/requisitos-atendimento';
import {ParametrosBaseService} from './parametros-base/parametros-base.service';
import {LogService} from '../log/log.service';
import {Log} from '../log/models/log';
import {MatSnackBar} from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class MonthCalcService {
  card: Modulo[];
  emerg: Modulo[];
  psico: Modulo[];
  neuro: Modulo[];
  pedi: Modulo[];
  medicos: Medico[];
  curLog: Log;

  constructor(private parametrosService: ParametrosBaseService,
              private log: LogService,
              private snackBar: MatSnackBar) {
  }

  updateFilters(modulos_owned: Modulo[], medicos: Medico[]) {
    this.card = modulos_owned.filter(modulo => modulo.nome === 'Cardiologia');
    this.emerg = modulos_owned.filter(modulo => modulo.nome === 'Emergência');
    this.psico = modulos_owned.filter(modulo => modulo.nome === 'Psicologia');
    this.neuro = modulos_owned.filter(modulo => modulo.nome === 'Neurologia');
    this.pedi = modulos_owned.filter(modulo => modulo.nome === 'Pediatria');
    this.medicos = medicos;
    this.curLog = this.log.startLog();
  }

  geraPaciente() {
    const A = new ClasseSocial({
      card: {media: 200, desvio: 8},
      emerg: {media: 100, desvio: 5},
      psico: {media: 250, desvio: 15},
      neuro: {media: 100, desvio: 5},
      pedi: {media: 200, desvio: 10},
    });

    const B = new ClasseSocial({
      card: {media: 100, desvio: 5},
      emerg: {media: 100, desvio: 5},
      psico: {media: 100, desvio: 5},
      neuro: {media: 100, desvio: 5},
      pedi: {media: 100, desvio: 5},
    });

    const C = new ClasseSocial({
      card: {media: 75, desvio: 8},
      emerg: {media: 100, desvio: 5},
      psico: {media: 100, desvio: 5},
      neuro: {media: 75, desvio: 8},
      pedi: {media: 100, desvio: 5},
    });

    const D = new ClasseSocial({
      card: {media: 50, desvio: 3},
      emerg: {media: 100, desvio: 5},
      psico: {media: 50, desvio: 3},
      neuro: {media: 50, desvio: 3},
      pedi: {media: 100, desvio: 5},
    });

    const E = new ClasseSocial({
      card: {media: 50, desvio: 3},
      emerg: {media: 100, desvio: 5},
      psico: {media: 50, desvio: 3},
      neuro: {media: 50, desvio: 3},
      pedi: {media: 100, desvio: 5},
    });
    this.curLog.logDemanda('card', {A: A.card, B: B.card, C: C.card, D: D.card, E: E.card});
    this.curLog.logDemanda('emerg', {A: A.emerg, B: B.emerg, C: C.emerg, D: D.emerg, E: E.emerg});
    this.curLog.logDemanda('psico', {A: A.psico, B: B.psico, C: C.psico, D: D.psico, E: E.psico});
    this.curLog.logDemanda('neuro', {A: A.neuro, B: B.neuro, C: C.neuro, D: D.neuro, E: E.neuro});
    this.curLog.logDemanda('pedi', {A: A.pedi, B: B.pedi, C: C.pedi, D: D.pedi, E: E.pedi});
    return {A: A, B: B, C: C, D: D, E: E};
  }

  medico_salario_indicador() {
    let salario_medicos = 0;
    this.medicos.forEach(
      medico => salario_medicos += medico.salario
    );
    this.curLog.logSalarioMedicos(salario_medicos);
    return salario_medicos;
  }

  medico_velocidade_indicador() {
    let velocidade_medicos = 0;
    this.medicos.forEach(
      medico => velocidade_medicos += (medico.pontualidade === 'A' || medico.pontualidade === 'a') ? 3 :
        ((medico.pontualidade === 'B' || medico.pontualidade === 'b') ? 2 : 1)
    );
    velocidade_medicos /= (this.medicos.length ? this.medicos.length : 1);
    if (this.medicos.length >= 10) {
      velocidade_medicos *= 1.1;
    }
    return velocidade_medicos;
  }

  medico_especialidade_indicador() {
    let especialidade_medico = 0;
    this.medicos.forEach(
      medico => especialidade_medico += (medico.expertise === 'A' || medico.expertise === 'a') ? 3 :
        ((medico.expertise === 'B' || medico.expertise === 'b') ? 2 : 1)
    );
    especialidade_medico /= (this.medicos.length ? this.medicos.length : 1);
    return especialidade_medico;
  }

  modulo_custo_indicador() {
    let custo_card = 0;
    let custo_emerg = 0;
    let custo_psico = 0;
    let custo_neuro = 0;
    let custo_pedi = 0;

    this.card.forEach(modulo => custo_card += modulo.custo_por_mes);
    this.emerg.forEach(modulo => custo_emerg += modulo.custo_por_mes);
    this.psico.forEach(modulo => custo_psico += modulo.custo_por_mes);
    this.neuro.forEach(modulo => custo_neuro += modulo.custo_por_mes);
    this.pedi.forEach(modulo => custo_pedi += modulo.custo_por_mes);

    this.curLog.logCustoMensal(custo_card, custo_emerg, custo_psico, custo_neuro, custo_pedi);
    return {card: custo_card, emerg: custo_emerg, psico: custo_psico, neuro: custo_neuro, pedi: custo_pedi};
  }

  modulo_preco_indicador() {
    let preco_card = 0;
    let preco_emerg = 0;
    let preco_psico = 0;
    let preco_neuro = 0;
    let preco_pedi = 0;

    this.card.forEach(modulo => preco_card += modulo.preco_do_tratamento);
    this.emerg.forEach(modulo => preco_emerg += modulo.preco_do_tratamento);
    this.psico.forEach(modulo => preco_psico += modulo.preco_do_tratamento);
    this.neuro.forEach(modulo => preco_neuro += modulo.preco_do_tratamento);
    this.pedi.forEach(modulo => preco_pedi += modulo.preco_do_tratamento);

    preco_card /= this.card.length ? this.card.length : 1;
    preco_emerg /= this.emerg.length ? this.emerg.length : 1;
    preco_psico /= this.psico.length ? this.psico.length : 1;
    preco_neuro /= this.neuro.length ? this.neuro.length : 1;
    preco_pedi /= this.pedi.length ? this.pedi.length : 1;

    this.curLog.logValorMedioAtendimento({
      card: preco_card,
      emerg: preco_emerg,
      psico: preco_psico,
      neuro: preco_neuro,
      pedi: preco_pedi
    });
    return {card: preco_card, emerg: preco_emerg, psico: preco_psico, neuro: preco_neuro, pedi: preco_pedi};
  }

  modulo_tecnologia_indicador() {
    let tecnologia_card = 0;
    let tecnologia_emerg = 0;
    let tecnologia_psico = 0;
    let tecnologia_neuro = 0;
    let tecnologia_pedi = 0;

    this.card.forEach(modulo => tecnologia_card += (modulo.tecnologia === 'A' || modulo.tecnologia === 'a') ? 3 :
      ((modulo.tecnologia === 'B' || modulo.tecnologia === 'b') ? 2 : 1));

    this.emerg.forEach(modulo => tecnologia_emerg += (modulo.tecnologia === 'A' || modulo.tecnologia === 'a') ? 3 :
      ((modulo.tecnologia === 'B' || modulo.tecnologia === 'b') ? 2 : 1));

    this.psico.forEach(modulo => tecnologia_psico += (modulo.tecnologia === 'A' || modulo.tecnologia === 'a') ? 3 :
      ((modulo.tecnologia === 'B' || modulo.tecnologia === 'b') ? 2 : 1));

    this.neuro.forEach(modulo => tecnologia_neuro += (modulo.tecnologia === 'A' || modulo.tecnologia === 'a') ? 3 :
      ((modulo.tecnologia === 'B' || modulo.tecnologia === 'b') ? 2 : 1));

    this.pedi.forEach(modulo => tecnologia_pedi += (modulo.tecnologia === 'A' || modulo.tecnologia === 'a') ? 3 :
      ((modulo.tecnologia === 'B' || modulo.tecnologia === 'b') ? 2 : 1));

    tecnologia_card /= this.card.length ? this.card.length : 1;
    tecnologia_emerg /= this.emerg.length ? this.emerg.length : 1;
    tecnologia_psico /= this.psico.length ? this.psico.length : 1;
    tecnologia_neuro /= this.neuro.length ? this.neuro.length : 1;
    tecnologia_pedi /= this.pedi.length ? this.pedi.length : 1;


    return {
      card: tecnologia_card, emerg: tecnologia_emerg, psico: tecnologia_psico,
      neuro: tecnologia_neuro, pedi: tecnologia_pedi
    };
  }

  modulo_conforto_indicador() {
    let conforto_card = 0;
    let conforto_emerg = 0;
    let conforto_psico = 0;
    let conforto_neuro = 0;
    let conforto_pedi = 0;

    this.card.forEach(modulo => conforto_card += (modulo.conforto === 'A' || modulo.conforto === 'a') ? 3 :
      ((modulo.conforto === 'B' || modulo.conforto === 'b') ? 2 : 1));

    this.emerg.forEach(modulo => conforto_emerg += (modulo.conforto === 'A' || modulo.conforto === 'a') ? 3 :
      ((modulo.conforto === 'B' || modulo.conforto === 'b') ? 2 : 1));

    this.psico.forEach(modulo => conforto_psico += (modulo.conforto === 'A' || modulo.conforto === 'a') ? 3 :
      ((modulo.conforto === 'B' || modulo.conforto === 'b') ? 2 : 1));

    this.neuro.forEach(modulo => conforto_neuro += (modulo.conforto === 'A' || modulo.conforto === 'a') ? 3 :
      ((modulo.conforto === 'B' || modulo.conforto === 'b') ? 2 : 1));

    this.pedi.forEach(modulo => conforto_pedi += (modulo.conforto === 'A' || modulo.conforto === 'a') ? 3 :
      ((modulo.conforto === 'B' || modulo.conforto === 'b') ? 2 : 1));

    conforto_card /= this.card.length ? this.card.length : 1;
    conforto_emerg /= this.emerg.length ? this.emerg.length : 1;
    conforto_psico /= this.psico.length ? this.psico.length : 1;
    conforto_neuro /= this.neuro.length ? this.neuro.length : 1;
    conforto_pedi /= this.pedi.length ? this.pedi.length : 1;

    return {
      card: conforto_card, emerg: conforto_emerg, psico: conforto_psico,
      neuro: conforto_neuro, pedi: conforto_pedi
    };
  }

  modulo_capacidade_indicador() {
    let capacidade_card = 0;
    let capacidade_emerg = 0;
    let capacidade_psico = 0;
    let capacidade_neuro = 0;
    let capacidade_pedi = 0;

    this.card.forEach(modulo => capacidade_card += modulo.capacidade);
    this.emerg.forEach(modulo => capacidade_emerg += modulo.capacidade);
    this.psico.forEach(modulo => capacidade_psico += modulo.capacidade);
    this.neuro.forEach(modulo => capacidade_neuro += modulo.capacidade);
    this.pedi.forEach(modulo => capacidade_pedi += modulo.capacidade);

    return {
      card: capacidade_card, emerg: capacidade_emerg, psico: capacidade_psico,
      neuro: capacidade_neuro, pedi: capacidade_pedi
    };
  }

  pacientesPerSocialClass(pacientes: number, capacidade: number, preco: number, tecnologia: number, conforto: number,
                          especialidade: number, velocidade: number, requisitos: RequisitosAtendimento) {

    if (
      capacidade <= 0 ||
      preco > requisitos.maxPreco ||
      especialidade < requisitos.minEspecialidade ||
      tecnologia < requisitos.minTecnologia ||
      conforto < requisitos.minConforto ||
      velocidade < requisitos.minVelocidade
    ) {
      return {atendidos: 0, capacidade: capacidade};
    }

    let atendidos;

    if (pacientes > capacidade) {
      atendidos = capacidade;
      capacidade = 0;
    } else {
      atendidos = pacientes;
      capacidade -= atendidos;
    }
    return {atendidos: atendidos, capacidade: capacidade};
  }

  pacientesAtendidos(pacientes: any, modulo: string, preco: number, capacidade: number, conforto: number,
                     tecnologia: number, velocidade: number, especialidade: number) {
    const A = pacientes.A[modulo];
    const B = pacientes.B[modulo];
    const C = pacientes.C[modulo];
    const D = pacientes.D[modulo];
    const E = pacientes.E[modulo];

    let atendidos_A;
    let atendidos_B;
    let atendidos_C;
    let atendidos_D;
    let atendidos_E;

    this.parametrosService.updateRequisitosAtendimento();
    const parametros = this.parametrosService.requisitosAtendimento;

    let response: { atendidos: number, capacidade: number };

    response = this.pacientesPerSocialClass(A, capacidade, preco, tecnologia, conforto, especialidade, velocidade, parametros.A);
    atendidos_A = response.atendidos;
    capacidade = response.capacidade;

    response = this.pacientesPerSocialClass(B, capacidade, preco, tecnologia, conforto, especialidade, velocidade, parametros.B);
    atendidos_B = response.atendidos;
    capacidade = response.capacidade;

    response = this.pacientesPerSocialClass(C, capacidade, preco, tecnologia, conforto, especialidade, velocidade, parametros.C);
    atendidos_C = response.atendidos;
    capacidade = response.capacidade;

    response = this.pacientesPerSocialClass(D, capacidade, preco, tecnologia, conforto, especialidade, velocidade, parametros.D);
    atendidos_D = response.atendidos;
    capacidade = response.capacidade;

    response = this.pacientesPerSocialClass(E, capacidade, preco, tecnologia, conforto, especialidade, velocidade, parametros.E);
    atendidos_E = response.atendidos;
    this.curLog.logAtendidos(modulo, {
      A: atendidos_A, B: atendidos_B, C: atendidos_C, D: atendidos_D, E: atendidos_E
    });
    return (atendidos_A + atendidos_B + atendidos_C + atendidos_D + atendidos_E);
  }

  logMes(mes: number) {
    return this.curLog.logMes(mes);
  }

  logCustoAquisicao(valor: number) {
    return this.curLog.logCustoAquisicao(valor);
  }

  logGanhosVendaModulos(valor: number) {
    return this.curLog.logGanhosVendaModulos(valor);
  }

  logDinheiroAcumulado(valor: number) {
    return this.curLog.logDinheiroAcumulado(valor);
  }

  logGanhosAtendimento(ganhos: {
    card: number;
    emerg: number;
    psico: number;
    neuro: number;
    pedi: number;
  }) {
    this.curLog.logGanhosAtendimento(ganhos);
  }

  finishCalc() {
    this.curLog.calculaLucro();
    this.log.saveLog(this.curLog);
    if (this.curLog.lucro > 0) {
      this.snackBar.open('O mês terminou, o Hospital lucrou R$ '
        + this.curLog.lucro.toFixed(2),
        'fechar', {duration: 5000});
    } else if (this.curLog.lucro < 0) {
      this.snackBar.open('O mês terminou, o Hospital obteve um prejuízo de R$ '
        + this.curLog.lucro.toFixed(2),
        'fechar',
        {duration: 5000});
    } else {
      this.snackBar.open('O mês terminou, o Hospital apenas recuperou seus gastos. Lucro: R$ '
        + this.curLog.lucro.toFixed(2),
        'fechar', {duration: 5000});
    }
  }
}
