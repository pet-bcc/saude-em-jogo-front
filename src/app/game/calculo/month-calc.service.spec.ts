import { TestBed } from '@angular/core/testing';

import { MonthCalcService } from './month-calc.service';

describe('MonthCalcService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MonthCalcService = TestBed.get(MonthCalcService);
    expect(service).toBeTruthy();
  });
});
