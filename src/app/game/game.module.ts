import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CountdownModule} from 'ngx-countdown';

import {GameConfigModule} from './game-config/game-config.module';
import {MedicoModule} from './medico/medico.module';
import {ModuloModule} from './modulo/modulo.module';
import {GameMainPageComponent} from './game-main-page/game-main-page.component';
import {RouterModule} from '@angular/router';
import {SharedModule} from '../shared/shared.module';
import {EmprestimoModule} from './emprestimo/emprestimo.module';
import {CalculoModule} from './calculo/calculo.module';
import {AppMaterialModule} from '../app-material.module';
import {EstatisticaModule} from './estatistica/estatistica.module';
import {NavComponent} from './nav/nav.component';
import {SaveScoreComponent} from './save-score/save-score.component';
import {ReactiveFormsModule} from '@angular/forms';
import {NgxCaptchaModule} from 'ngx-captcha';


@NgModule({
  declarations: [
    GameMainPageComponent,
    NavComponent,
    SaveScoreComponent
  ],
  imports: [
    CommonModule,
    GameConfigModule,
    MedicoModule,
    ModuloModule,
    EmprestimoModule,
    RouterModule,
    CountdownModule,
    CalculoModule,
    EstatisticaModule,
    ReactiveFormsModule,
    NgxCaptchaModule,
    AppMaterialModule,
    SharedModule
  ],
  exports: [
    GameConfigModule,
    MedicoModule,
    ModuloModule,
    EmprestimoModule,
    CountdownModule,
    CalculoModule
  ],
  entryComponents: [
    SaveScoreComponent
  ]
})
export class GameModule {
}
