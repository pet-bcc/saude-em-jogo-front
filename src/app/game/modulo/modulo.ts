export interface Modulo {
  id: number;
  rodada_aquisicao?: number;
  nome: string;
  tecnologia: string;
  conforto: string;
  custo_por_mes: number;
  capacidade: number;
  custo_aquisicao: number;
  preco_do_tratamento: number;
}
