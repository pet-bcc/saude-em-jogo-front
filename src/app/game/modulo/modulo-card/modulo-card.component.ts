import {Component, Input, OnInit} from '@angular/core';

import {Modulo} from '../modulo';
import {GameService} from '../../game.service';

@Component({
  selector: 'sj-modulo-card',
  templateUrl: './modulo-card.component.html',
  styleUrls: ['./modulo-card.component.scss']
})
export class ModuloCardComponent implements OnInit {
  @Input() modulo: Modulo;
  @Input() isMine: boolean;

  constructor(private game: GameService) { }

  ngOnInit() {
  }

  get money() {
    return this.game.money;
  }

  get config() {
    return this.game.configuration;
  }

  buyModule(id: number) {
    this.game.buyModule(id);
  }

  sellModule(id: number) {
    this.game.sellModulo(id);
  }

  canSell() {
    return this.game.current_round <= this.game.current_round;
  }

  getBarValue(char: string) {
    switch (char) {
      case 'A' || 'a':
        return 100;
      case 'B' || 'b':
        return (100 / 3 * 2);
      case 'C' || 'c':
        return (100 / 3);
      default:
        return 0;
    }
  }
}
