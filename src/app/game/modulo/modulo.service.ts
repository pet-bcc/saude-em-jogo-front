import {Injectable} from '@angular/core';
import {ApiService} from '../../shared/services/api.service';
import {Observable} from 'rxjs';
import {Modulo} from './modulo';


@Injectable({
  providedIn: 'root'
})
export class ModuloService {

  constructor(private _api: ApiService) {
  }

  getAll(): Observable<{ modulos: Modulo[] }> {
    return this._api.get('all_modulos') as Observable<{ modulos: Modulo[] }>;
  }
}
