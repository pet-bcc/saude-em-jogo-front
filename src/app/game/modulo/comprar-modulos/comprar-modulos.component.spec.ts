import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComprarModulosComponent } from './comprar-modulos.component';

describe('ComprarModulosComponent', () => {
  let component: ComprarModulosComponent;
  let fixture: ComponentFixture<ComprarModulosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComprarModulosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComprarModulosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
