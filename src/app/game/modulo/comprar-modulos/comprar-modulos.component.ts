import { Component, OnInit } from '@angular/core';
import {GameService} from '../../game.service';

@Component({
  selector: 'sj-comprar-modulos',
  templateUrl: './comprar-modulos.component.html',
  styleUrls: ['./comprar-modulos.component.scss']
})
export class ComprarModulosComponent implements OnInit {

  constructor(private game: GameService) { }

  get modulos() {
    return this.game.modulos_available;
  }

  ngOnInit() {
  }

}
