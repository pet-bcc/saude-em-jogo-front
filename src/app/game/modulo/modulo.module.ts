import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ModuloService} from './modulo.service';
import { ModuloCardComponent } from './modulo-card/modulo-card.component';
import {SharedModule} from '../../shared/shared.module';
import {AppMaterialModule} from '../../app-material.module';
import { ModuloAllComponent } from './modulo-all/modulo-all.component';
import { ComprarModulosComponent } from './comprar-modulos/comprar-modulos.component';
import { MeusModulosComponent } from './meus-modulos/meus-modulos.component';
import {MedicoModule} from '../medico/medico.module';

@NgModule({
  declarations: [
    ModuloCardComponent,
    ModuloAllComponent,
    ComprarModulosComponent,
    MeusModulosComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AppMaterialModule,
    MedicoModule
  ],
  exports: [
    ModuloAllComponent
  ],
  providers: [
    ModuloService
  ]
})
export class ModuloModule { }
