import { Component, OnInit } from '@angular/core';

import {GameService} from '../../game.service';

@Component({
  selector: 'sj-meus-modulos',
  templateUrl: './meus-modulos.component.html',
  styleUrls: ['./meus-modulos.component.scss']
})
export class MeusModulosComponent implements OnInit {

  constructor(private game: GameService) { }

  get modulos() {
    return this.game.modulos_owned;
  }

  ngOnInit() {
  }

}
