import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeusModulosComponent } from './meus-modulos.component';

describe('MeusModulosComponent', () => {
  let component: MeusModulosComponent;
  let fixture: ComponentFixture<MeusModulosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeusModulosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeusModulosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
