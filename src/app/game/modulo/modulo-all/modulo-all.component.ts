import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Observable} from 'rxjs';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {map} from 'rxjs/operators';

import {Modulo} from '../modulo';
import {animate, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'sj-modulo-all',
  templateUrl: './modulo-all.component.html',
  styleUrls: ['./modulo-all.component.scss'],
  animations: [
    trigger('items', [
      // cubic-bezier for a tiny bouncing feel
      transition(':enter', [
        style({ transform: 'scale(0.5)', opacity: 0 }),
        animate('1s cubic-bezier(.8,-0.6,0.2,1.5)',
          style({ transform: 'scale(1)', opacity: 1 }))
      ]),
      transition(':leave', [
        style({ transform: 'scale(1)', opacity: 1, height: '*' }),
        animate('1s cubic-bezier(.8,-0.6,0.2,1.5)',
          style({ transform: 'scale(0.5)', opacity: 0, height: '0px', margin: '0px' }))
      ]),
    ])
  ]
})
export class ModuloAllComponent implements OnInit, OnChanges {
  @Input() modulos: Modulo[];
  @Input() isMine = false;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );
  constructor(private breakpointObserver: BreakpointObserver) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.modulos && !changes.modulos.isFirstChange()) {
      this.modulos = changes.modulos.currentValue;
    }
    if (changes.isMine && !changes.isMine.isFirstChange()) {
      this.isMine = changes.isMine.currentValue;
    }
  }

}
