import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModuloAllComponent } from './modulo-all.component';

describe('ModuloAllComponent', () => {
  let component: ModuloAllComponent;
  let fixture: ComponentFixture<ModuloAllComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModuloAllComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModuloAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
