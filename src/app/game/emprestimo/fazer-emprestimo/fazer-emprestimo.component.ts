import { Component, OnInit } from '@angular/core';
import {GameService} from '../../game.service';

@Component({
  selector: 'sj-fazer-emprestimo',
  templateUrl: './fazer-emprestimo.component.html',
  styleUrls: ['./fazer-emprestimo.component.scss']
})
export class FazerEmprestimoComponent implements OnInit {

  constructor(private game: GameService) { }

  get emprestimos_disponiveis() {
    return this.game.emprestimos_available;
  }

  ngOnInit() {
  }

}
