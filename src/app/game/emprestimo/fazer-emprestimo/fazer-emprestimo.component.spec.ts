import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FazerEmprestimoComponent } from './fazer-emprestimo.component';

describe('FazerEmprestimoComponent', () => {
  let component: FazerEmprestimoComponent;
  let fixture: ComponentFixture<FazerEmprestimoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FazerEmprestimoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FazerEmprestimoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
