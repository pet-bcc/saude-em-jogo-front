import { Component, OnInit } from '@angular/core';
import {GameService} from '../../game.service';

@Component({
  selector: 'sj-meus-emprestimos',
  templateUrl: './meus-emprestimos.component.html',
  styleUrls: ['./meus-emprestimos.component.scss']
})
export class MeusEmprestimosComponent implements OnInit {

  constructor(private game: GameService) { }

  get meus_emprestimos() {
    return this.game.emprestimos_made;
  }

  get total() {
    let total = 0;
    const emprestimos = this.meus_emprestimos;
    emprestimos.forEach(
      emprestimo => {
        total += emprestimo.valor;
      }
    );
    return total;
  }

  ngOnInit() {
  }

}
