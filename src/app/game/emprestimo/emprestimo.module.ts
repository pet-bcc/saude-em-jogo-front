import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmprestimoItemComponent } from './emprestimo-item/emprestimo-item.component';
import { AppMaterialModule } from 'src/app/app-material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { EmprestimoListaComponent } from './emprestimo-lista/emprestimo-lista.component';
import { MeusEmprestimosComponent } from './meus-emprestimos/meus-emprestimos.component';
import { FazerEmprestimoComponent } from './fazer-emprestimo/fazer-emprestimo.component';

@NgModule({
  declarations: [EmprestimoItemComponent, EmprestimoListaComponent, MeusEmprestimosComponent, FazerEmprestimoComponent],
  imports: [
    CommonModule,
    AppMaterialModule,
    SharedModule
  ]
})
export class EmprestimoModule { }
