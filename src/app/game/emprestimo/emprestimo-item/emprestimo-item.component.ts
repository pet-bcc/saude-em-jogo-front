import { Component, OnInit, Input } from '@angular/core';
import { Emprestimo } from '../emprestimo';
import { GameService } from '../../game.service';

@Component({
  selector: 'sj-emprestimo-item',
  templateUrl: './emprestimo-item.component.html',
  styleUrls: ['./emprestimo-item.component.scss']
})
export class EmprestimoItemComponent implements OnInit {
  @Input() emprestimo: Emprestimo;
  constructor(private game: GameService) { }

  makeEmprestimo(valor: number) {
    this.game.makeEmprestimo(valor);
  }

  ngOnInit() {
  }

}
