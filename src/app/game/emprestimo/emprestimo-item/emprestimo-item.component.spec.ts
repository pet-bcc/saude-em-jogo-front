import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmprestimoItemComponent } from './emprestimo-item.component';

describe('EmprestimoItemComponent', () => {
  let component: EmprestimoItemComponent;
  let fixture: ComponentFixture<EmprestimoItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmprestimoItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmprestimoItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
