import {Component, Input, OnInit} from '@angular/core';

import {Emprestimo} from '../emprestimo';

@Component({
  selector: 'sj-emprestimo-lista',
  templateUrl: './emprestimo-lista.component.html',
  styleUrls: ['./emprestimo-lista.component.scss']
})
export class EmprestimoListaComponent implements OnInit {
  @Input() emprestimos: Emprestimo[];
  @Input() titulo: string;
  constructor() { }

  ngOnInit() {
  }

}
