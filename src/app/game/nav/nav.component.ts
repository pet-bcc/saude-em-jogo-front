import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {GameService} from '../game.service';

@Component({
  selector: 'sj-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  get money() {
    return this.game.money;
  }

  get finished() {
    return this.game.finished;
  }

  constructor(private breakpointObserver: BreakpointObserver,
              private game: GameService) {}

}
