import { Injectable } from '@angular/core';
import {LogMes} from './models/log-mes';
import {Log} from './models/log';
import {ApiService} from '../../shared/services/api.service';
import {stringify} from 'querystring';

@Injectable({
  providedIn: 'root'
})
export class LogService {
  private _logs: LogMes[] = [];
  constructor(private api: ApiService) { }

  getLog(mes: number): LogMes {
    return this._logs.find( log => log.referente_mes === mes );
  }

  getAll() {
    return this._logs;
  }

  startLog() {
    return new Log();
  }

  saveLog(log: LogMes) {
    this._logs.push(log);
  }

  submitLog(name, captcha) {
    const rank = this._logs;
    return this.api.post('submit_alternative_rank',
      JSON.stringify({ rank: { rank, name, resultado: rank[rank.length - 1].dinheiro_acumulado } , 'g-recaptcha-response': captcha }));
  }
}
