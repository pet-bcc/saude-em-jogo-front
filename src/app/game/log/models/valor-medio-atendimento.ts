export interface ValorMedioAtendimento {
  card: number;
  emerg: number;
  psico: number;
  neuro: number;
  pedi: number;
}
