import {Gastos} from './gastos';
import {ValorMedioAtendimento} from './valor-medio-atendimento';
import {AtendimentosPorModulo} from './atendimentos-por-modulo';
import {Ganhos} from './ganhos';

export interface LogMes {
  referente_mes: number;
  gastos: Gastos;
  valor_medio_atendimento: ValorMedioAtendimento;
  atendidos: AtendimentosPorModulo;
  ganhos: Ganhos;
  lucro: number;
  dinheiro_acumulado: number;
}
