export interface Gastos {
  salario_medicos: number;
  custo_mensal: {
    card: number;
    emerg: number;
    psico: number;
    neuro: number;
    pedi: number;
  };
  custo_aquisicao: number;
}
