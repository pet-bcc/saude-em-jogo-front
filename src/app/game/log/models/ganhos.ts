export interface Ganhos {
  venda_modulos: number;
  atendimento: {
    card: number;
    emerg: number;
    psico: number;
    neuro: number;
    pedi: number;
  };
}
