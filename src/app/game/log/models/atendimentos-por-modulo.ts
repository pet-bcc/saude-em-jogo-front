import {PacientesAtendidos} from './pacientes-atendidos';

export interface AtendimentosPorModulo {
  demanda: {
    card: PacientesAtendidos;
    emerg: PacientesAtendidos;
    psico: PacientesAtendidos;
    neuro: PacientesAtendidos;
    pedi: PacientesAtendidos;
  };
  card: PacientesAtendidos;
  emerg: PacientesAtendidos;
  psico: PacientesAtendidos;
  neuro: PacientesAtendidos;
  pedi: PacientesAtendidos;
}
