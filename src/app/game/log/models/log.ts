import {LogMes} from './log-mes';
import {AtendimentosPorModulo} from './atendimentos-por-modulo';
import {Ganhos} from './ganhos';
import {Gastos} from './gastos';
import {ValorMedioAtendimento} from './valor-medio-atendimento';
import {PacientesAtendidos} from './pacientes-atendidos';

export class Log implements LogMes {
  atendidos: AtendimentosPorModulo;
  dinheiro_acumulado: number;
  ganhos: Ganhos;
  gastos: Gastos;
  lucro: number;
  referente_mes: number;
  valor_medio_atendimento: ValorMedioAtendimento;

  constructor() {
    this.atendidos = { demanda: {} } as AtendimentosPorModulo;
    this.dinheiro_acumulado = 0;
    this.ganhos = { atendimento: {} } as Ganhos;
    this.gastos = { custo_mensal: {} } as Gastos;
    this.lucro = 0;
    this.referente_mes = 0;
    this.valor_medio_atendimento = {} as ValorMedioAtendimento;
  }

  logMes(mes: number) {
    this.referente_mes = mes;
  }

  logSalarioMedicos(salario: number) {
    this.gastos.salario_medicos = salario;
  }

  logCustoMensal(card: number,
                 emerg: number,
                 psico: number,
                 neuro: number,
                 pedi: number) {
    this.gastos.custo_mensal.card = card;
    this.gastos.custo_mensal.emerg = emerg;
    this.gastos.custo_mensal.psico = psico;
    this.gastos.custo_mensal.neuro = neuro;
    this.gastos.custo_mensal.pedi = pedi;
  }

  logCustoAquisicao(valor: number) {
    this.gastos.custo_aquisicao = valor;
  }

  logAtendidos(modulo: string, atendidos: PacientesAtendidos) {
    this.atendidos[modulo] = atendidos;
  }

  logDemanda(modulo: string, demanda: PacientesAtendidos) {
    this.atendidos.demanda[modulo] = demanda;
  }

  logValorMedioAtendimento(valores: ValorMedioAtendimento) {
    this.valor_medio_atendimento = valores;
  }

  logDinheiroAcumulado(money: number) {
    this.dinheiro_acumulado = money;
  }

  logGanhosAtendimento(ganhos: {
    card: number;
    emerg: number;
    psico: number;
    neuro: number;
    pedi: number;
  }) {
    this.ganhos.atendimento = ganhos;
  }

  logGanhosVendaModulos(ganhos: number) {
    this.ganhos.venda_modulos = ganhos;
  }

  calculaLucro() {
    let gastos = 0;
    gastos += this.gastos.salario_medicos;
    gastos += this.gastos.custo_mensal.card;
    gastos += this.gastos.custo_mensal.emerg;
    gastos += this.gastos.custo_mensal.psico;
    gastos += this.gastos.custo_mensal.neuro;
    gastos += this.gastos.custo_mensal.pedi;
    gastos += this.gastos.custo_aquisicao;
    let total = 0;
    total += this.ganhos.atendimento.card;
    total += this.ganhos.atendimento.emerg;
    total += this.ganhos.atendimento.psico;
    total += this.ganhos.atendimento.neuro;
    total += this.ganhos.atendimento.pedi;
    total += this.ganhos.venda_modulos;
    this.lucro = total - gastos;
  }
}
