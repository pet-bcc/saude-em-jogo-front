import {Injectable} from '@angular/core';

import {Medico} from './medico/medico';
import {Config} from './game-config/config';
import {Modulo} from './modulo/modulo';
import {GameConfigService} from './game-config/game-config.service';
import {MedicoService} from './medico/medico.service';
import {ModuloService} from './modulo/modulo.service';
import {Emprestimo} from './emprestimo/emprestimo';
import {MonthCalcService} from './calculo/month-calc.service';
import {Subject} from 'rxjs';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material';
import {SaveScoreComponent} from './save-score/save-score.component';


@Injectable({
  providedIn: 'root'
})
export class GameService {
  finished = false;
  onNextMonth = new Subject();

  configuration: Config;

  money = 0;

  current_round = 1;

  medicos_available: Medico[] = [];
  medicos_contracted: Medico[] = [];

  modulos_available: Modulo[] = [];
  modulos_owned: Modulo[] = [];

  emprestimos_available: Emprestimo[] = [];
  emprestimos_made: Emprestimo[] = [];

  log_aquisicao: number[] = [0];
  log_venda: number[] = [0];

  constructor(private config: GameConfigService,
              private medicos: MedicoService,
              private modulos: ModuloService,
              private calc: MonthCalcService,
              private router: Router,
              private dialog: MatDialog) {
  }

  startGame(callback: (_: boolean) => void) {
    this.emprestimos_available = [];
    this.current_round = 1;
    this.medicos.getAll()
      .subscribe(result => {
        this.medicos_available = result.medicos;
      });
    this.modulos.getAll()
      .subscribe(result => {
        this.modulos_available = result.modulos;
      });
    this.config.getOficialConfig()
      .subscribe(result => {
        this.configuration = result;
        this.money = this.configuration.caixa_inicial;
        this.configuration.emprestimos_possiveis.forEach(
          val => {
            this.emprestimos_available.push({
              valor: parseInt(val, 10),
              rodada_aquisicao: -1,
              disable: false
            });
          }
        );
        callback(true);
      });
  }

  toContractMedico(id: number) {
    const contracted = this.medicos_available.find(element => element.id === id);
    contracted.rodada_aquisicao = this.current_round;
    this.medicos_available.splice(this.medicos_available.findIndex(element => element.id === id), 1);
    this.medicos_contracted.push(contracted);
  }

  toDismissMedico(id: number) {
    const dismissed = this.medicos_contracted.find(element => element.id === id);
    this.medicos_contracted.splice(this.medicos_contracted.findIndex(element => element.id === id), 1);
    this.medicos_available.push(dismissed);
  }

  buyModule(id: number) {
    const bought = this.modulos_available.find(element => element.id === id);
    this.money -= bought.custo_aquisicao;
    this.log_aquisicao.push(bought.custo_aquisicao);
    this.modulos_available.splice(this.modulos_available.findIndex(element => element.id === id), 1);
    this.modulos_owned.push(bought);
  }

  sellModulo(id: number) {
    const selled = this.modulos_owned.find(element => element.id === id);
    this.money += (selled.custo_aquisicao * (this.configuration.porcentagem_reembolso_modulo / 100));
    this.log_venda.push((selled.custo_aquisicao * (this.configuration.porcentagem_reembolso_modulo / 100)));
    this.modulos_owned.splice(this.modulos_owned.findIndex(element => element.id === id), 1);
    this.modulos_available.push(selled);
  }

  makeEmprestimo(valor: number) {
    const emprestimo = this.emprestimos_available.find(element => element.valor === valor);
    emprestimo.disable = true;
    const novo_emprestimo: Emprestimo = {
      valor: emprestimo.valor,
      rodada_aquisicao: this.current_round,
      disable: true
    };
    this.money += emprestimo.valor;
    this.emprestimos_made.push(novo_emprestimo);
  }

  nextMonth() {
    this.calc.updateFilters(this.modulos_owned, this.medicos_contracted);
    this.calc.logMes(this.current_round);
    this.calc.logCustoAquisicao(this.log_aquisicao.reduce((total, num) => total + num));
    this.log_aquisicao = [0];
    this.calc.logGanhosVendaModulos(this.log_venda.reduce((total, num) => total + num));
    this.log_venda = [0];

    if (this.current_round === this.configuration.mes_pagar_emprestimo) {
      let pagar_emprestimo = 0;
      this.emprestimos_made.forEach(emprestimo => pagar_emprestimo += emprestimo.valor);
      this.money -= pagar_emprestimo * (this.configuration.juros_do_emprestimo / 100 + 1);
    }

    const salario_medicos = this.calc.medico_salario_indicador();
    const velocidade_medicos = this.calc.medico_velocidade_indicador();
    const especialidade_medico = this.calc.medico_especialidade_indicador();

    const modulo_custo = this.calc.modulo_custo_indicador();
    const modulo_preco = this.calc.modulo_preco_indicador();
    const modulo_tecnologia = this.calc.modulo_tecnologia_indicador();
    const modulo_conforto = this.calc.modulo_conforto_indicador();
    const modulo_capacidade = this.calc.modulo_capacidade_indicador();

    const pacientes = this.calc.geraPaciente();

    const card_atendidos = this.calc.pacientesAtendidos(pacientes, 'card', modulo_preco.card,
      modulo_capacidade.card, modulo_conforto.card, modulo_tecnologia.card, velocidade_medicos, especialidade_medico );
    const emerg_atendidos = this.calc.pacientesAtendidos(pacientes, 'emerg', modulo_preco.emerg,
      modulo_capacidade.emerg, modulo_conforto.emerg, modulo_tecnologia.emerg, velocidade_medicos, especialidade_medico );
    const psico_atendidos = this.calc.pacientesAtendidos(pacientes, 'psico', modulo_preco.psico,
      modulo_capacidade.psico, modulo_conforto.psico, modulo_tecnologia.psico, velocidade_medicos, especialidade_medico );
    const neuro_atendidos = this.calc.pacientesAtendidos(pacientes, 'neuro', modulo_preco.neuro,
      modulo_capacidade.neuro, modulo_conforto.neuro, modulo_tecnologia.neuro, velocidade_medicos, especialidade_medico );
    const pedi_atendidos = this.calc.pacientesAtendidos(pacientes, 'pedi', modulo_preco.pedi,
      modulo_capacidade.pedi, modulo_conforto.pedi, modulo_tecnologia.pedi, velocidade_medicos, especialidade_medico );

    const ganhos_atendimento = {
      card: (card_atendidos * modulo_preco.card),
      emerg: (emerg_atendidos * modulo_preco.emerg),
      psico: (psico_atendidos * modulo_preco.psico),
      neuro: (neuro_atendidos * modulo_preco.neuro),
      pedi: (pedi_atendidos * modulo_preco.pedi)
    };

    this.calc.logGanhosAtendimento({
      card: ganhos_atendimento.card,
      emerg: ganhos_atendimento.emerg,
      psico: ganhos_atendimento.psico,
      neuro: ganhos_atendimento.neuro,
      pedi: ganhos_atendimento.pedi
    });

    this.money += ganhos_atendimento.card;
    this.money += ganhos_atendimento.emerg;
    this.money += ganhos_atendimento.psico;
    this.money += ganhos_atendimento.neuro;
    this.money += ganhos_atendimento.pedi;

    let modulo_custo_total = 0;
    modulo_custo_total += modulo_custo.card;
    modulo_custo_total += modulo_custo.emerg;
    modulo_custo_total += modulo_custo.psico;
    modulo_custo_total += modulo_custo.neuro;
    modulo_custo_total += modulo_custo.pedi;

    this.money -= modulo_custo_total;
    this.money -= salario_medicos;

    this.calc.logDinheiroAcumulado(this.money);
    this.calc.finishCalc();

    this.emprestimos_available.forEach(emprestimo => emprestimo.disable = false);

    if (this.current_round < 8) {
      this.current_round++;
    } else {
      this.finished = true;
      this.router.navigate(['game']);
      const dialogRef = this.dialog.open(SaveScoreComponent, { maxWidth: '350px' });
    }
    this.onNextMonth.next(true);
  }
}
