import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {LogService} from '../log/log.service';
import * as Config from '../../shared/config';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'sj-save-score',
  templateUrl: './save-score.component.html',
  styleUrls: ['./save-score.component.scss']
})
export class SaveScoreComponent implements OnInit {
  newScore = this.fb.group({
    nome: ['', [Validators.required, Validators.maxLength(30)]],
    captcha: ['', Validators.required]
  });
  siteKey = Config.siteKey;

  constructor(private fb: FormBuilder,
              private log: LogService,
              private dialogRef: MatDialogRef<SaveScoreComponent>) { }

  ngOnInit() {
  }

  submit() {
    if (!this.newScore.valid) {
      return;
    }
    this.log.submitLog(this.newScore.getRawValue().nome, this.newScore.getRawValue().captcha)
      .subscribe();
    this.dialogRef.close();
  }

}
