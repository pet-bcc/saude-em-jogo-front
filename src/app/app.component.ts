import {Component, OnInit} from '@angular/core';
import {GameService} from './game/game.service';
import {Router, NavigationEnd} from '@angular/router';

@Component({
  selector: 'sj-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private game: GameService,
              private router: Router) {
      this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        (<any>window).ga('set', 'page', event.urlAfterRedirects);
        (<any>window).ga('send', 'pageview');
      }
    });
  }

  ngOnInit(): void {
  }
}
