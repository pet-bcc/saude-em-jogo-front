import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ApiService} from './services/api.service';
import {CardDefaultComponent} from './components/card-default/card-default.component';
import {AppMaterialModule} from '../app-material.module';
import {BrMoneyPipe} from './pipes/br-money.pipe';
import {LayoutModule} from '@angular/cdk/layout';
import {RouterModule} from '@angular/router';
import {ConfirmComponent} from './components/confirm/confirm.component';


@NgModule({
  declarations: [
    CardDefaultComponent,
    BrMoneyPipe,
    ConfirmComponent,
  ],
  imports: [
    CommonModule,
    AppMaterialModule,
    LayoutModule,
    RouterModule,
  ],
  exports: [
    CardDefaultComponent,
    BrMoneyPipe,
    ConfirmComponent,
  ],
  providers: [
    ApiService
  ]
})
export class SharedModule {
}
