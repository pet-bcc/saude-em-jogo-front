import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'sj-card-default',
  templateUrl: './card-default.component.html',
  styleUrls: ['./card-default.component.scss']
})
export class CardDefaultComponent implements OnInit {
  @Input() title: string;
  @Input() subtitle: string;
  @Input() avatarImg: string;
  constructor() { }

  ngOnInit() {
  }

}
