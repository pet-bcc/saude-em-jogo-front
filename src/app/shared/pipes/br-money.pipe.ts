import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'brMoney'
})
export class BrMoneyPipe implements PipeTransform {

  transform(value: number): string {
    const num = value.toFixed(2);
    const split = num.split('.');
    return `R$ ${split[0]},${split[1]}`;
  }

}
