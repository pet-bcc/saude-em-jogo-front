import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

const API_URL = 'https://saudeemjogo.gq/api/';

interface Options {
  headers?: HttpHeaders | { [header: string]: string | string[]; };
  observe?: any;
  params?: HttpParams | { [param: string]: string | string[]; };
  reportProgress?: boolean;
  responseType?: 'json';
  withCredentials?: true;
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private _http: HttpClient) {
  }

  get(url: string, options?: Options) {
    return this._http.get(API_URL + url, options);
  }

  post(url: string, body?: any, options?: Options) {
    return this._http.post(API_URL + url, body, options);
  }

  headers(values?: { name: string, value: string }) {
    const headers = new HttpHeaders();
    headers.append('content-type', 'application/json');
    headers.append('responseType', 'json');
    return headers;
  }
}
