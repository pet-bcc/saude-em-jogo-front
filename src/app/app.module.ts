import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {CountdownModule} from 'ngx-countdown';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {SharedModule} from './shared/shared.module';
import {AppMaterialModule} from './app-material.module';
import {GameModule} from './game/game.module';
import {IndexComponent} from './index/index.component';

// function countdownConfigFactory(): Config {
//   return { template: `$!h!:$!m!:$!s!` };
// }

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AppMaterialModule,
    SharedModule,
    HttpClientModule,
    GameModule,
    CountdownModule,
  ],
  providers: [
    // { provide: CountdownConfig, useFactory: countdownConfigFactory }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
